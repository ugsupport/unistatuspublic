---
        # plik należy umieścić w katalogu content/issues
        # należy wypełnić pola title oraz date
        # można również uzupełnić opis awarii w przypadku prywatnej strony statusowej
        # plik należy zacommitować i wypchnąć do repo z użytkownika ugsupport lub innego mjącego uprawnienia do repo
        # po zakończeniu awarii należy zmienić resolved na true oraz
        # odkomentować parametr resolvedWhen i wpisa odpowiednią datę
        # jeśli nie jest konieczne trzymanie historii awarii plik można usunąć z repozytorium

        title: Awaria Jira
        date: %%data awarii%%
        resolved: false
        # Possible severity levels: down, disrupted, notice
        severity: down
        affected:
        - JIRA
        section: issue
        # resolvedWhen: %%data zakonczenia%%

        # dla private page można odkomentować:
        # informational: true
        # pin: true
---

Nie działa Jira, status pozostałych systemów może być nieaktualny.


